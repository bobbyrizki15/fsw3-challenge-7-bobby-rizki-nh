import React from "react";
import "../../assets/css/StylePage.css"
import "bootstrap/dist/css/bootstrap.min.css";
import LogoMobil from "../../assets/img/car.png";

export default function ContentMainNoButton(){
    return(
        <div className="container-head ">
            <div className="container-fluid ">
                <div className="home-section-one">
                    <div className="left ">
                        <div className="card-body ">
                            <h2 className="this-header card-title ">
                                Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h2>
                            <p className="paragraph card-text mt-3 ">
                                Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas 
                                terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
                            </p>
                        </div>
                    </div>
                    <div className="right">
                        <img src={LogoMobil} alt="images-car " className="images-car " />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    )
}
