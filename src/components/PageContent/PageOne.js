import React from "react";
import "../../assets/css/StylePage.css"
import "bootstrap/dist/css/bootstrap.min.css";
import LogoMobil from "../../assets/img/car.png";
import {Link} from "react-router-dom";
import "../../assets/css/responsive.css"

export default function ContentMain(){
    return(
        <div className="container-head ">
            <div className="container-fluid ">
                <div className="home-section-one">
                    <div className="left ">
                        <div className="card-body ">
                            <h2 className="this-header card-title ">
                                Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h2>
                            <p className="paragraph card-text mt-3 ">
                                Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas 
                                terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
                            </p>
                            <Link to="/Cars">
                                <div className="this-header paragraph ">
                                    <button className=" button-green margin-button btn btn-success 
                                        card-text " >Mulai Sewa Mobil
                                    </button>
                                </div>
                            </Link>
                        </div>
                    </div>
                    <div className="right">
                        <img src={LogoMobil} alt="images-car " className="images-car " />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    )
}