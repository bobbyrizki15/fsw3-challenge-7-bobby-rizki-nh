import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/StylePage.css";
import iconComplete from "../../assets/img/icon_complete.png";
import iconPrice from "../../assets/img/icon_price.png";
import iconProf from "../../assets/img/icon_professional.png";
import iconOneDay from "../../assets/img/icon_24hrs.png";

export default function WhyUs(){
    return (
        <div class="container-main " id="why-us">
        <div class="container-fluid ">
            <div class="why-us">
                <h2 class="this-header mt-2" style={{lineHeight: "36px", fontSize: "24px"}}>
                    Why Us?
                </h2>
                <p class=" paragraph mt-2 ">Mengapa harus pilih Binar Car Rental?</p>
                <div class="card-only " style={{marginRight: "25px"}}>
                    <div class="card border-why-us bg-light " >
                        <div class="card-body ">
                            <img src={iconComplete} alt=" " />
                            <h2 class="card-text card-heading pt-3 ">Mobil Lengkap</h2>
                            <p class="card-text paragraph pt-1 side-card-text-text ">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                        </div>
                    </div>
                </div>
                <div class="card-only " style={{marginRight: "25px"}}>
                    <div class="card border-why-us bg-light ">
                        <div class="card-body ">
                            <img src={iconPrice} alt=" " />
                            <h2 class="card-text card-heading pt-3 ">Harga Murah</h2>
                            <p class="card-text paragraph pt-1 side-card-text ">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                        </div>
                    </div>
                </div>
                <div class="card-only " style={{marginRight: "25px"}}>
                    <div class="card border-why-us bg-light ">
                        <div class="card-body ">
                            <img src={iconOneDay} alt=" " />
                            <h2 class="card-text card-heading pt-3 " >Layanan 24 Jam</h2>
                            <p class="card-text paragraph pt-1 side-card-text ">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                        </div>
                    </div>
                </div>
                <div class="card-only "style={{marginRight: "25px"}}>
                    <div class="card border-why-us bg-light  ">
                        <div class="card-body ">
                            <img src={iconProf} alt=" " />
                            <h2 class="card-text card-heading pt-3 ">Sopir Profesional</h2>
                            <p class="card-text paragraph pt-1 side-card-text ">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    )
}