import React from "react";
import "../../assets/css/StylePage.css";
import "bootstrap/dist/css/bootstrap.min.css";
import iconFacebook from "../../assets/img/icon_facebook.png";
import iconInsta from "../../assets/img/icon_instagram.png";
import iconMail from "../../assets/img/icon_mail.png";
import iconTwitch from "../../assets/img/icon_twitch.png";
import iconTwitter from "../../assets/img/icon_twitter.png";
import {Link} from "react-router-dom";

export default function Footer(){
    return(
        <div class=" container-fluid ">
            <div class=" footer-main mt-5 ">
                <div class=" footer-list ">
                    <li class=" paragraph ">Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000
                    </li>
                    <li class=" paragraph  ">binarcarrental@gmail.com</li>
                    <li class=" paragraph  ">081-233-334-808</li>
                </div>
                <div class=" footer-list ">
                    <li class=" paragraph ">Our services</li>
                    <li class=" paragraph  ">Why Us </li>
                    <li class=" paragraph  ">Testinomial</li>
                    <li class=" paragraph  ">FAQ</li>
                </div>
                <div class=" footer-list ">
                    <li class=" paragraph ">Connect with us</li>
                    <div class=" row mt-3 ">
                        <div class=" col-1 "> <img src={iconFacebook} alt=" "/></div>
                        <div class=" col-1 " style={{marginLeft: "12px"}}><img src={iconInsta} alt=" "/></div>
                        <div class=" col-1 " style={{marginLeft: "12px"}}><img src={iconTwitter} alt=" "/></div>
                        <div class=" col-1 " style={{marginLeft: "12px"}}><img src={iconMail} alt=" "/></div>
                        <div class=" col-1 " style={{marginLeft: "12px"}}><img src={iconTwitch} alt=" "/></div>
                    </div>
                </div>
                <div class=" footer-list">
                    <p class=" paragraph ">Copyright Binar 2022</p>
                    <Link to="/">
                      <div class=" footer-button ">
                        <button type=" button " class="footer-button-blue text-white btn btn-primary btn-outline-success "> Home
                        </button>
                      </div>
                    </Link>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    )
}