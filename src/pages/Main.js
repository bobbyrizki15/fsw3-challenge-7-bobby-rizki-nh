import React from "react";
import "../assets/css/responsive.css"
import ContentBanner from "../components/banner/Banner";
import Header from "../components/header/header";
import OurService from "../components/ourservice/OurService";
import ContentMain from "../components/PageContent/PageOne";
import  WhyUs from "../components/whyus/WhyUs";
import ContentFaq from "../components/faq/Faq";
import Footer from "../components/footer/Footer";

const index = () => {
    return (
        <div>
        <Header/>
        <ContentMain/>
        <OurService/>
        <WhyUs/>
        <ContentBanner/>
        <ContentFaq/>
        <Footer/>
        </div>
    )
}

export default index
