import React from 'react';
import "./App.css";
import "./assets/css/responsive.css"
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./assets/css/responsive.css"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Main from "./pages/Main";

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
      <Routes>
        <Route path="/" element ={<Main />}/>
      </Routes>
    </BrowserRouter>
   

    );
  }
}
export default App;
